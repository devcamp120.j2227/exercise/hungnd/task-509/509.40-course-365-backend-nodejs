// Import thư viện expressjs tương đương import express from "express";
const express = require("express");
// Import thư viện path
const path = require("path");
// Khởi tạo 1 app express
const app = express();
// Import thu viện mongoose
const mongoose = require('mongoose');
// khai báo cổng chạy project
const port = 8000;
//Callback function là 1 function đóng vài trò là tham số của 1 function khác, nó sẽ thực hiện khi function chủ được gọi
// Khai báo API dạng /
app.get("/", (req, res) => {
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/views/index.html"));
});
// Khai báo để sử dụng tài nguyên tĩnh
app.use(express.static(__dirname + "/views"));

// Khai báo để sử dụng bodyJson
app.use(express.json());

// Khai báo để sử dụng UTF-8
app.use(express.urlencoded({
    extended: true
}));

// Import courseRouter 
const { courseRouter } = require("./app/routes/courseRouter");
app.use("/api", courseRouter);
// Kết nối với mongoose
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Course365", (error) => {
    if (error) throw error;
    console.log('Successfully connected');
});
app.use((req, res, next) => {
    console.log("Current time: ", new Date());
    next();
})
app.listen(port, () => {
    console.log("App listening on port: ", port);
});