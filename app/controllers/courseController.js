// Khai báo thư viện mongoose
const mongoose = require("mongoose");
// Khai báo model
const courseModel = require('../model/courseModel');

// create a courses
const createCourses = (req, res) => {
    //B1: Thu thập dữ liệu
    const bodyCourse = req.body;
    console.log(bodyCourse);
    //B2: Kiểm tra dữ liệu
    // Kiểm tra courseCode
    if (!bodyCourse.courseCode) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "courseCode is not valid!"
        });
    }
    // Kiểm tra courseName
    if (!bodyCourse.courseName) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "courseName is not valid!"
        })
    }
    // Kiểm tra price
    if (!bodyCourse.price || isNaN(bodyCourse.price) || bodyCourse.price < 0) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "price is not valid!"
        })
    }
    // Kiểm tra discountPrice
    if (!bodyCourse.discountPrice || isNaN(bodyCourse.discountPrice) || bodyCourse.discountPrice < 0) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "discountPrice is not valid!"
        })
    }
    // Kiểm tra duration
    if (!bodyCourse.duration) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "duration is not valid!"
        })
    }
    // Kiểm tra level
    if (!bodyCourse.level) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "level is not valid!"
        })
    }
    // Kiểm tra coverImage
    if (!bodyCourse.coverImage) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "coverImage is not valid!"
        })
    }
    // Kiểm tra teacherName
    if (!bodyCourse.teacherName) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "teacherName is not valid!"
        })
    }
    // Kiểm tra teacherPhoto
    if (!bodyCourse.teacherPhoto) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "teacherPhoto is not valid!"
        })
    }
    //B3: thực hiện tạo mới course
    let newCourse = {
        _id: mongoose.Types.ObjectId(),
        courseCode: bodyCourse.courseCode,
        courseName: bodyCourse.courseName,
        price: bodyCourse.price,
        discountPrice: bodyCourse.discountPrice,
        duration: bodyCourse.duration,
        level: bodyCourse.level,
        coverImage: bodyCourse.coverImage,
        teacherName: bodyCourse.teacherName,
        teacherPhoto: bodyCourse.teacherPhoto,
        isPopular: bodyCourse.isPopular,
        isTrending: bodyCourse.isTrending
    }

    courseModel.create(newCourse, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Create new courses successfully!",
                "data": data
            })
        }
    })
};

// get all courses
const getAllCourses = (req, res) => {
    //B1: Thu thập dữ liệu(không cần)
    //B2: Kiểm tra dữ liệu(không cần)
    //B3: Thực hiện tạo mới course
    courseModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Load all courses successfully!",
                "data": data
            })
        }
    })
};
// get a course
const getCourseById = (req, res) => {
    //B1: Thu thập dữ liệu
    const coursesId = req.params.coursesId;
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(coursesId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Course Id is not valid!"
        })
    }
    // B3: Thực hiện load course theo id
    courseModel.findById(coursesId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Get courses by Id successfully!",
                "data": data
            })
        }
    })
};
// update course by Id
const updateCourseById = (req, res) => {
    //B1: Thu thập dữ liệu
    const coursesId = req.params.coursesId;
    console.log(coursesId);

    let bodyCourse = req.body;
    console.log(bodyCourse);

    //B2: Kiểm tra dữ liệu
    // KIểm tra course Id
    if (!mongoose.Types.ObjectId.isValid(coursesId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Course Id is not valid!"
        })
    }
    // Kiểm tra courseCode
    if (!bodyCourse.courseCode) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "courseCode is not valid!"
        });
    }
    // Kiểm tra courseName
    if (!bodyCourse.courseName) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "courseName is not valid!"
        })
    }
    // Kiểm tra price
    if (!bodyCourse.price || isNaN(bodyCourse.price) || bodyCourse.price < 0) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "price is not valid!"
        })
    }
    // Kiểm tra discountPrice
    if (!bodyCourse.discountPrice || isNaN(bodyCourse.discountPrice) || bodyCourse.discountPrice < 0) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "discountPrice is not valid!"
        })
    }
    // Kiểm tra duration
    if (!bodyCourse.duration) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "duration is not valid!"
        })
    }
    // Kiểm tra level
    if (!bodyCourse.level) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "level is not valid!"
        })
    }
    // Kiểm tra coverImage
    if (!bodyCourse.coverImage) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "coverImage is not valid!"
        })
    }
    // Kiểm tra teacherName
    if (!bodyCourse.teacherName) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "teacherName is not valid!"
        })
    }
    // Kiểm tra teacherPhoto
    if (!bodyCourse.teacherPhoto) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "teacherPhoto is not valid!"
        })
    }
    //B3: thực hiện tạo mới course
    let newCourse = {
        courseCode: bodyCourse.courseCode,
        courseName: bodyCourse.courseName,
        price: bodyCourse.price,
        discountPrice: bodyCourse.discountPrice,
        duration: bodyCourse.duration,
        level: bodyCourse.level,
        coverImage: bodyCourse.coverImage,
        teacherName: bodyCourse.teacherName,
        teacherPhoto: bodyCourse.teacherPhoto,
        isPopular: bodyCourse.isPopular,
        isTrending: bodyCourse.isTrending
    }
    courseModel.findByIdAndUpdate(coursesId, newCourse, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Update courses by Id successfully!",
                "data": data
            })
        }
    })
};

// delete a course by Id
const deleteCourseById = (req, res) => {
    //B1: Thu thập dữ liệu
    const coursesId = req.params.coursesId;
    console.log(coursesId);
    // B2: Kiểm tra dữ liệu
    // KIểm tra course Id
    if (!mongoose.Types.ObjectId.isValid(coursesId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Course Id is not valid!"
        })
    }
    // B3: Thực hiện xóa course theo id
    courseModel.findByIdAndDelete(coursesId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Delete courses by Id successfully!",
                "data": data
            })
        }
    })
}
module.exports = {
    createCourses,
    getAllCourses,
    getCourseById,
    updateCourseById,
    deleteCourseById
}