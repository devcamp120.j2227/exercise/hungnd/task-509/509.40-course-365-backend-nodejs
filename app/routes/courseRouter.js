// khai báo thư viện express
const express = require('express');

// Import middleware
const courseMiddleware = require('../middlewares/courseMiddleware');

// Tạo ra Router
const courseRouter = express.Router();
// dùng use sẽ truyền hết mỗi lần chạy
//courseRouter.use(getAllCoursesMiddleware);
// Import course controller
const courseController = require('../controllers/courseController');
// create new course
courseRouter.post('/courses', courseMiddleware.postCoursesMiddleware, courseController.createCourses);
// get all course
courseRouter.get('/courses', courseMiddleware.getAllCoursesMiddleware, courseController.getAllCourses);
// get a course by id
courseRouter.get('/courses/:coursesId', courseMiddleware.getCoursesMiddleware, courseController.getCourseById);
// update a course by id
courseRouter.put('/courses/:coursesId', courseMiddleware.putCoursesMiddleware, courseController.updateCourseById);
// delete a course by id
courseRouter.delete('/courses/:coursesId', courseMiddleware.deleteCoursesMiddleware, courseController.deleteCourseById);

module.exports = { courseRouter }