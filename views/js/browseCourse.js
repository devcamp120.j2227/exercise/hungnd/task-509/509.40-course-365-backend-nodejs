
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
import { gCoursesDB } from "./script.js";
// biến toàn cục chứa các mảng course
var gCoursesObj = gCoursesDB.courses;
// biến lưu trữ dữ Id course
var gCoursesId = 0;
// Biến mảng hằng số chứa danh sách tên các thuộc tính
const ARRAY_NAME = ['stt', 'coverImage', 'courseCode', 'courseName', 'level', 'duration', 'price', 'discountPrice', 'teacherName', 'teacherPhoto', 'isPopular', 'isTrending', 'action'];
// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const COL_STT = 0;
const COL_COVER_IMAGE = 1;
const COL_COURSE_CODE = 2;
const COL_COURSE_NAME = 3;
const COL_LEVEL = 4;
const COL_DURATION = 5;
const COL_PRICE = 6;
const COL_DISCOUNT_PRICE = 7;
const COL_TEACHER_NAME = 8;
const COL_TEACHER_PHOTO = 9;
const COL_IS_POPULAR = 10;
const COL_IS_TRENDING = 11;
const COL_ACTION = 12;
// định nghĩa table
var gCoursesTable = $("#course-table").DataTable({
    columns: [
        { data: ARRAY_NAME[COL_STT] },
        { data: ARRAY_NAME[COL_COVER_IMAGE] },
        { data: ARRAY_NAME[COL_COURSE_CODE] },
        { data: ARRAY_NAME[COL_COURSE_NAME] },
        { data: ARRAY_NAME[COL_LEVEL] },
        { data: ARRAY_NAME[COL_DURATION] },
        { data: ARRAY_NAME[COL_PRICE] },
        { data: ARRAY_NAME[COL_DISCOUNT_PRICE] },
        { data: ARRAY_NAME[COL_TEACHER_NAME] },
        { data: ARRAY_NAME[COL_TEACHER_PHOTO] },
        { data: ARRAY_NAME[COL_IS_POPULAR] },
        { data: ARRAY_NAME[COL_IS_TRENDING] },
        { data: ARRAY_NAME[COL_ACTION] }
    ],
    columnDefs: [
        {
            targets: COL_STT,
            render: function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        },
        {
            targets: COL_COVER_IMAGE,
            render: function (data) {
                return '<img src="' + data + '" width="100px" height="100px">'
            }
        },
        {
            targets: COL_PRICE,
            render: function (data) {
                return data + "$"
            }
        },
        {
            targets: COL_DISCOUNT_PRICE,
            render: function (data) {
                return data + "$"
            }
        },
        {
            targets: COL_TEACHER_PHOTO,
            render: function (data) {
                return '<img src="' + data + '" width="50px" height="50px" class="rounded-circle">'
            }
        },
        {
            targets: COL_IS_POPULAR,
            render: function (data) {
                return getChecked(data);
            }
        },
        {
            targets: COL_IS_TRENDING,
            render: function (data) {
                return getChecked(data);
            }
        },
        {
            targets: COL_ACTION,
            defaultContent: 
                `<button class="btn btn-info btn-update-course" title="Edit"><i class="far fa-edit"></i></button>
                <button class="btn btn-danger btn-delete-course" title="Delete"><i class="far fa-trash-alt"></i></button>`
        }
    ]
});
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    // gán eventhandler cho nút thêm khóa học
    $("#create-course").click(function () {
        onBtnCreateCourseClick();
    });
    // gán eventhandler cho nút confirm create-modal
    $("#btn-confirm-create").click(function () {
        onBtnConfirmCreateClick();
    });
    // gán eventhandler cho nút sửa-update khóa học btn-delete-course
    $("#course-table").on('click', '.btn-update-course', function () {
        onBtnUpdateCourseClick(this);
    });
    // gán eventhandler cho nút delete khóa học 
    $("#course-table").on('click', '.btn-delete-course', function () {
        onBtnDeleteCourseClick(this);
    });
    // gán eventhandler cho nút xác nhận sửa dữ liệu -modal
    $("#btn-confirm-update").click(function () {
        onBtnConfirmUpdateCourseClick();
    });
    // gán sự kiện cho nút xác nhận xóa course-modal
    $("#btn-confirm-delete").click(function () {
        onBtnConfirmDeleteClick();
    });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm xử lý khi tải trang
function onPageLoading() {
    "use strict";
    console.log("Tải trang tại đây");
    // gọi hàm load dữ liệu ra table
    loadDataCourseToTable(gCoursesObj);
}
// Hàm xử lý khi nhấn nút thêm khóa hoc
function onBtnCreateCourseClick() {
    "use strict";
    console.log("Nút Create Course được ấn");
    // hiện modal thêm khóa học
    $("#create-course-modal").modal('show');

}
// Hàm xử lý khi nhấn nút confirm create - modal
function onBtnConfirmCreateClick() {
    "use strict";
    // định nghĩa đối tượng cần xử lý
    var vCreateCourseObj = {
        id: 0,
        courseCode: "",
        courseName: "",
        price: 0,
        discountPrice: 0,
        duration: "",
        level: "",
        coverImage: "",
        teacherName: "",
        teacherPhoto: "",
        isPopular: "",
        isTrending: ""
    };
    // B1: Thu thập dữ liệu
    getDataCreateCourse(vCreateCourseObj);
    // B2: Kiểm tra dữ liệu
    var vValidate = validataDataCreateCourse(vCreateCourseObj);
    if (vValidate == true) {
        //B3: truyền dữ liệu vào coursedatabase
        gCoursesObj.push(vCreateCourseObj);
        console.log(gCoursesObj);
        alert("Thêm khóa học thành công");
        $("#create-course-modal").modal('hide'); // ẩn modal create course
        clearDataModalCreateCourse();// xóa trắng thông tin input modal create
        loadDataCourseToTable(gCoursesObj); // load dữ liệu vào bảng
    }
}
// Hàm xử lý khi nhấn nút sửa-update course
function onBtnUpdateCourseClick(paramButton) {
    "use strict";
    console.log("Nút được ấn");
    // hiện modal chỉnh sửa-update course
    $("#update-course-modal").modal('show');
    // lấy thông tin đối tượng trên hàng chưa button được ấn
    var vRowCurrent = $(paramButton).closest('tr');
    var vRowData = gCoursesTable.row(vRowCurrent).data();
    gCoursesId = vRowData.id; // gán ID course vào biến cục bộ
    // load dữ liệu vào modal
    loadDataCourseToModalUpdate(vRowData);
}
// Hàm xử lý khi nút confirm update được ấn- modal update
function onBtnConfirmUpdateCourseClick() {
    "use strict";
    console.log("Nút được ấn");
    // khai báo đối tượng cần xử lý
    var vUpdateCourseObj = {
        id: 0,
        courseCode: "",
        courseName: "",
        price: 0,
        discountPrice: 0,
        duration: "",
        level: "",
        coverImage: "",
        teacherName: "",
        teacherPhoto: "",
        isPopular: "",
        isTrending: ""
    };
    //B1: Thu thập dữ liệu
    getDataCourseUpdate(vUpdateCourseObj);
    //B2: Kiểm tra dữ liệu
    var vValidate = validateValueUpdate(vUpdateCourseObj);
    if (vValidate == true) {
        //B3: Update course vào database
        updateCourse(vUpdateCourseObj);
        // load lại dữ liệu table
        loadDataCourseToTable(gCoursesObj);
        alert("Update thành công");
        // xóa trắng modal update
        clearDataModalUpdateCourse();
        // ẩn modal update
        $("#update-course-modal").modal('hide');
    }
}
// Hàm xử lý khi nhấn nút xóa course
function onBtnDeleteCourseClick(paramButton) {
    "use strict";
    // hiện modal delete
    $("#delete-course-modal").modal('show');
    // lấy thông tin đối tượng trên hàng chưa button được ấn
    var vRowCurrent = $(paramButton).closest('tr');
    var vRowData = gCoursesTable.row(vRowCurrent).data();
    gCoursesId = vRowData.id; // gán ID course vào biến cục bộ
}
// Hàm xử lý khi nhấn nút confirm xóa-modal
function onBtnConfirmDeleteClick() {
    "use strict";
    //B1: Thu thập dữ liệu(không cần)
    //B2: Kiểm tra dữ liệu(không cần)
    //B3: Xóa đối tượng course
    deleteCourse(gCoursesId);
    alert("Xóa thành công");
    // ẩn modal
    $("#delete-course-modal").modal('hide');
    // load lại dữ liệu table
    loadDataCourseToTable(gCoursesObj);
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// Hàm xử lý render lại cột Ispopular và istrending
function getChecked(paramData) {
    "use strict";
    var vCheck = `<input type="checkbox" checked="checked" disabled: true>`;
    if (!paramData)
        vCheck = `<input type="checkbox' disabled:true>`;
    return vCheck;
}
// Hàm thu thập dữ liệu trên modal thêm khóa học
function getDataCreateCourse(paramCourseObj) {
    "use strict";
    paramCourseObj.id = getNextId();
    paramCourseObj.courseCode = $.trim($("#inp-course-code").val());
    paramCourseObj.courseName = $.trim($("#inp-course-name").val());
    paramCourseObj.price = $.trim($("#inp-price").val());
    paramCourseObj.discountPrice = $.trim($("#inp-discount-price").val());
    paramCourseObj.duration = $.trim($("#inp-duration").val());
    paramCourseObj.level = $("#select-level").val();
    paramCourseObj.coverImage = $.trim($("#inp-cover-image").val());
    paramCourseObj.teacherName = $("#select-teacher-name").val();
    paramCourseObj.teacherPhoto = $.trim($("#inp-teacher-photo").val());
    var vKiemTraBooleanPopurlar = false;
    if ($("#select-popular").val() == "true") {
        vKiemTraBooleanPopurlar = true;
    }
    paramCourseObj.isPopular = $("#select-popular").val();
    var vKiemTraBooleanTrending = false;
    if ($("#select-trending").val() == "true") {
        vKiemTraBooleanTrending = true;
    }
    paramCourseObj.isTrending = vKiemTraBooleanTrending;
}
// Hàm kiểm tra dữ liệu trên modal thêm khóa học
function validataDataCreateCourse(paramCourseObj) {
    "use strict";
    if (paramCourseObj.courseCode === "" || paramCourseObj.courseName === "" || paramCourseObj.price === ""
        || paramCourseObj.discountPrice === "" || paramCourseObj.duration === "" || paramCourseObj.coverImage === "" || paramCourseObj.teacherPhoto === "") {
        alert("Trường không được để trống");
        return false;
    }
    return true;
}
// hàm sinh ra đc id tự tăng tiếp theo, dùng khi Thêm mới phần tử
function getNextId() {
    var vNextId = 0;
    // Nếu mảng chưa có đối tượng nào thì Id = 1
    if (gCoursesObj.length == 0) {
        vNextId = 1;
    }
    // Id tiếp theo bằng Id của phần tử cuối cùng + thêm 1    
    else {
        vNextId = gCoursesObj[gCoursesObj.length - 1].id + 1;
    }
    return vNextId;
}
// Hàm load dữ liệu vào table
function loadDataCourseToTable(paramCourseObj) {
    "use strict";
    gCoursesTable.clear();
    gCoursesTable.rows.add(paramCourseObj);
    gCoursesTable.draw();
}
// Hàm xóa trắng bảng input trên modal create course
function clearDataModalCreateCourse() {
    $("#inp-course-code").val('');
    $("#inp-course-name").val('');
    $("#inp-price").val('');
    $("#inp-discount-price").val('');
    $("#inp-duration").val('');
    $("#inp-cover-image").val('');
    $("#inp-teacher-photo").val('');
}
// Hàm load thông tin khóa học lên modal update
function loadDataCourseToModalUpdate(paramCourseObj) {
    "use strict";
    $("#inp-course-code-update").val(paramCourseObj.courseCode).attr('disabled', 'disabled');// không cho sửa coursecode
    $("#inp-course-name-update").val(paramCourseObj.courseName);
    $("#inp-price-update").val(paramCourseObj.price);
    $("#inp-discount-price-update").val(paramCourseObj.discountPrice);
    $("#inp-duration-update").val(paramCourseObj.duration);
    $("#select-level-update").val(paramCourseObj.level);
    $("#inp-cover-image-update").val(paramCourseObj.coverImage);
    $("#select-teacher-name-update").val(paramCourseObj.teacherName);
    $("#inp-teacher-photo-update").val(paramCourseObj.teacherPhoto);
    var vGetValueIsPopular = paramCourseObj.isPopular;
    if (vGetValueIsPopular) {
        $("#select-popular-update").val("true");
    }
    else {
        $("#select-popular-update").val("false");
    }
    var vGetValueIsTrending = paramCourseObj.isTrending;
    if (vGetValueIsTrending) {
        $("#select-trending-update").val("true");
    }
    else {
        $("#select-trending-update").val("false");
    }
}
// Hàm thu thập dữ liệu từ modal update
function getDataCourseUpdate(paramInputUpdate) {
    "use strict";
    paramInputUpdate.id = gCoursesId;
    paramInputUpdate.courseCode = $.trim($("#inp-course-code-update").val());
    paramInputUpdate.courseName = $.trim($("#inp-course-name-update").val());
    paramInputUpdate.price = $("#inp-price-update").val();
    paramInputUpdate.discountPrice = $.trim($("#inp-discount-price-update").val());
    paramInputUpdate.duration = $.trim($("#inp-duration-update").val());
    paramInputUpdate.level = $("#select-level-update").val();
    paramInputUpdate.coverImage = $.trim($("#inp-cover-image-update").val());
    paramInputUpdate.teacherName = $("#select-teacher-name-update").val();
    paramInputUpdate.teacherPhoto = $.trim($("#inp-teacher-photo-update").val());
    var vKiemTraBooleanPopurlar = false;
    if ($("#select-popular").val() == "true") {
        vKiemTraBooleanPopurlar = true;
    }
    paramInputUpdate.isPopular = $("#select-popular").val();
    var vKiemTraBooleanTrending = false;
    if ($("#select-trending").val() == "true") {
        vKiemTraBooleanTrending = true;
    }
    paramInputUpdate.isTrending = vKiemTraBooleanTrending;
}
// Hàm kiểm tra dữ liệu update
function validateValueUpdate(paramInputUpdate) {
    "use strict";
    if (paramInputUpdate.courseCode === "" || paramInputUpdate.courseName === "" || paramInputUpdate.price === ""
        || paramInputUpdate.discountPrice === "" || paramInputUpdate.duration === "" || paramInputUpdate.coverImage === "" || paramInputUpdate.teacherPhoto === "") {
        alert("Trường không được để trống");
        return false;
    }
    return true;
}
// Hàm lấy course index từ course id
//input: paramCourseId là courseId cần tìm index
//output: trả về chỉ số index trong mảng course
function getCourseIndexFromId(paramCourseId) {
    "use strict";
    var vCourseIndex = -1;
    var vCourseFound = false;
    var vLoopIndex = 0;
    while (!vCourseFound && vLoopIndex < gCoursesObj.length) {
        if (gCoursesObj[vLoopIndex].id === paramCourseId) {
            vCourseIndex = vLoopIndex;
            vCourseFound = true;
        }
        else {
            vLoopIndex++;
        }
    }
    return vCourseIndex;
}
// Hàm thực hiện update course trong mảng
function updateCourse(paramCourseObj) {
    "use strict";
    // lấy chỉ số index trong mảng
    var vCourseIndex = getCourseIndexFromId(paramCourseObj.id);
    // xóa courseObj khỏi mảng
    gCoursesObj.splice(vCourseIndex, 1, paramCourseObj) // xóa từ vị trí index và xóa 1 phần tử và thêm phần tử mới
}
// Hàm xóa trắng bảng input trên modal update course
function clearDataModalUpdateCourse() {
    $("#inp-course-code-update").val('');
    $("#inp-course-name-update").val('');
    $("#inp-price-update").val('');
    $("#inp-discount-price-update").val('');
    $("#inp-duration-update").val('');
    $("#inp-cover-image-update").val('');
    $("#inp-teacher-photo-update").val('');
}
// Hàm thực hiện delete course trong mảng
function deleteCourse(paramCourseId) {
    "use strict";
    // lấy chỉ số index trong mảng
    var vCourseIndex = getCourseIndexFromId(paramCourseId);
    // xóa courseObj khỏi mảng
    gCoursesObj.splice(vCourseIndex, 1) // xóa từ vị trí index và xóa 1 phần tử
}
